"""
Testing the inverse model.


"""

from __future__ import print_function, division
import unittest
import math
import random

# replace by your forward model if you wish to.
# it needs to have a (1, 1, 1) orientation
# from fwdmodel import f_kin
from fwdmodel_num import f_kin_ref as f_kin
#from invmodel_ref import i_kin_ref as i_kin
from invmodel_num import i_kin_ref as i_kin

random.seed(0)
# acceptable position error in mm
POSITION_MARGIN = 20
# acceptable angle error in radians
ANGLE_MARGIN    = 10

def diff_angles(a, b):
    return 180 - abs(abs((a - b) % 360) - 180)

class TestInverseModel(unittest.TestCase):

    def implicit_enough(self, p):
        """We verify f_kin(i_kin(p)) == p"""
        for angles in i_kin(*p):
            p2 = f_kin(*angles)
            self.assertTrue(sum((p_i-p2_i)**2 for p_i, p2_i in zip(p, p2)) < POSITION_MARGIN)

    def explicit_enough(self, p, angles):
        """We verify that `angles` is in i_kin(p)"""
        min_error = float('inf')
        for angles_ikin in i_kin(*p):
            min_error = min(min_error, sum(diff_angles(a_i, a2_i)**2
                                          for a_i, a2_i in zip(angles, angles_ikin)))

        if min_error > ANGLE_MARGIN:
            print('ref  angles: {}'.format(angles))
            print('ikin angles: {}'.format(i_kin(*p)))
        self.assertTrue(min_error <= ANGLE_MARGIN)

    def dual_test(self, angles):
        self.implicit_enough(f_kin(*angles))
        self.explicit_enough(f_kin(*angles), angles)

    def test_zero_implicit(self):
        self.implicit_enough(f_kin(0, 0, 0))
    def test_zero_explicit(self):
        self.explicit_enough(f_kin(0, 0, 0), (0, 0, 0))

    def test_simple0_implicit(self):
        self.implicit_enough(f_kin(90,  0,  0))
    def test_simple0_explicit(self):
        self.explicit_enough(f_kin(90,  0,  0), (90,  0,  0))

    def test_simple1_implicit(self):
        self.implicit_enough(f_kin( 0, 90,  0))
    def test_simple1_explicit(self):
        self.explicit_enough(f_kin( 0, 90,  0), ( 0, 90,  0))

    def test_simple2_implicit(self):
        self.implicit_enough(f_kin( 0,  0, 90))
    def test_simple2_explicit(self):
        self.explicit_enough(f_kin( 0,  0, 90), ( 0,  0, 90))

    def test_joint0(self):
        self.dual_test((30,  0,  0))
        self.dual_test((45,  0,  0))
        self.dual_test((60,  0,  0))

    def test_joint1(self):
        self.dual_test(( 0, 30,  0))
        self.dual_test(( 0, 45,  0))
        self.dual_test(( 0, 60,  0))

    def test_joint2(self):
        self.dual_test(( 0,  0, 30))
        self.dual_test(( 0,  0, 45))
        self.dual_test(( 0,  0, 60))

    def test_random_joint1(self):
        for _ in range(20):
            self.dual_test((random.uniform(-180, 180),  0, 0))

    def test_random_joint23(self):
        for _ in range(20):
            self.dual_test((0, random.uniform(-180, 180), random.uniform(-180, 180)))

    def test_full_random(self):
        for _ in range(20):
            self.dual_test((random.uniform(-180, 180), random.uniform(-180, 180), random.uniform(-180, 180)))


if __name__ == '__main__':
    unittest.main()
